<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

$myemail = 'info@giostarmexico.com';//<-----Put Your email address here.
$bccmail = 'creativo@686studio.com, content1@sanidentalgroup.com';//<-----Put Your BCC email address here.

$name = $_POST['elName']; 
$email_address = $_POST['elEmail'];
$origin = $_POST['elOrigin'];
$phone = $_POST['elPhone'];
$where = $_POST['elWhere'];
$message = $_POST['elMessage'];
$utm = $_POST['field_utm_landing'];
$fieldHidden = isset($_POST['elAddress']) ? $_POST['elAddress'] : null;

if($name && $email_address && $message)
{
	$to = $myemail; 
	$email_subject = "$origin";
	$email_body = "You have received a new message of Giostar Mexico".
	" Here are the details:\n
	Name: $name \n
	Email: $email_address \n
	Phone: $phone \n
	Where are you from?: $where \n
	Message: \n $message \n
	UTM: $utm"; 
	
	$headers = "From: $email_address\n"; /*Campo del Email del cliente*/
	$headers .= "Reply-To: $email_address\n"; /*Campo del Email de respuesta*/
	$headers .= "Bcc: " . $bccmail; /*Campo del Email de copia oculta*/
	if(!$fieldHidden)
	mail($to,$email_subject,$email_body,$headers);
	//redirect to the 'thank you' page
	header('Location: /thanks.html');
} 
?>